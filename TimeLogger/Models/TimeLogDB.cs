﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TimeLogger.Models
{
    public class TimeLogDB : DbContext
    {
        public DbSet<TimeLogModel> TimeLogs { get; set; }
    }
}