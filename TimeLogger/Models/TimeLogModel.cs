﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TimeLogger.Models
{
    public class TimeLogModel
    {
        [Required]
        public int Id { get; set; }

        //Datum
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d MMM yyyy}")]
        public DateTime Date { get; set; }

        //Woran gearbeitet wurde
        public string WorkItem { get; set; }

        //Arbeitszeit
        public TimeSpan WorkingTime { get; set; }

        //gestoppte Zeit
        public TimeSpan StopTime { get; set; }
    }
}