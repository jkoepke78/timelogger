﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TimeLogger.Startup))]
namespace TimeLogger
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
