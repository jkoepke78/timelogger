﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeLogger.Models;

namespace TimeLogger.Controllers
{
    public class TimeLogController : Controller
    {
        private TimeLogDB db = new TimeLogDB();

        // GET: TimeLog
        public ActionResult Index()
        {
            
            DateTime currentDate = DateTime.Now;

            if (db.TimeLogs.ToList().Select(a => a.Date.Date.ToString("d").Equals(currentDate.Date.ToString("d"))).Count() == 0)
            {
                TimeLogModel timeLogModel = new TimeLogModel()
                {
                    Date = currentDate,
                    WorkItem = "",
                    WorkingTime = new TimeSpan(0),
                    StopTime = new TimeSpan(0)
                };
                return View(timeLogModel);
            }

            return View(db.TimeLogs.ToList().Select(a => a.Date.Date.ToString("d").Equals(currentDate.Date.ToString("d"))).First());
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult IndexPost(TimeLogModel prmTimeLog) 
        {
            prmTimeLog.Date = DateTime.Now;
            prmTimeLog.WorkingTime = prmTimeLog.WorkingTime.Add(prmTimeLog.StopTime);

            return View(prmTimeLog);
        }
    }
}
